# err-liquidsoap

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Errbot plugin to control audio streams using liquidsoap.

## Background

This plugin was started by the [JáquerEspeis](https://bunqueer.jaquerespeis.org) to control the stream of [Radiocronía](https://radiocronia.jaquerespeis.org).

## Prerequisites

Install liquidsoap in the server that runs the bot.

In Ubuntu:

```
sudo apt install liquidsoap
```

## Install

```
!repos install https://gitlab.com/jaquerespeis/err-liquidsoap
```

## Configuration

```
!plugin config Liquidsoap {'admin_group_id': 'change me', 'host': 'change me', 'port': 'change me', 'mount': 'change me', 'user': 'change me', 'password': 'change me'}
```

## Usage

Retransmit a radio stream:

```
/stream_radio <address>
```

Stream a playlist:

```
/stream_playlist <address> --duration <duration_in_minutes>
```

Stop liquidsoap:

```
/stop_liquidsoap
```

## Test

1. Install python and venv:

```
sudo apt install python3 python3-venv
```

2. Create and activate the virtual environment:

In the parent directory because of
https://github.com/errbotio/errbot/issues/1412#issuecomment-679622186

```
python3 -m venv ../.venv-errbot
source ../.venv-errbot/bin/activate
```

3. Install the dependencies:

```
pip install errbot testtools pytest
```

4. Run the tests:

```
python3 -m unittest discover tests/
```

## Maintainer

* [elopio](https://keybase.io/elopio)

## Contributing

TODO

## License

[GPLv3](LICENSE) (Copyleft) [JáquerEspeis](https://bunqueer.jaquerespeis.org)
