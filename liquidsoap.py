import os
import shutil
import subprocess
import tempfile
import urllib.parse
import urllib.request

import errbot


class LiquidsoapError(Exception):
    """Base exception class for liquidsoap errors."""


class LiquidsoapNotFromAdminError(Exception):
    """Exception raised when commands are not called from the admin group."""


class LiquidsoapNotRunningError(LiquidsoapError):
    """Exception raised when liquidsoap is expected to be running
    but is not.
    """

    pass


class LiquidsoapUnsupportedProtocolError(LiquidsoapError):
    """Exception raised when the protocol is not supported."""

    pass


class Liquidsoap(errbot.BotPlugin):
    """Errbot plugin to manage liquidsoap."""

    # Liquidsoap process. When not running is set to None.
    _liquidsoap_process = None
    # Path to directory with the liquidsoap scripts.
    _scripts_path = os.path.join(
        os.path.dirname(__file__),
        'scripts'
    )

    def get_configuration_template(self):
        return {
            'admin_group_id': 'change me',
            'host': 'change me',
            'port': 'change me',
            'mount': 'change me',
            'user': 'change me',
            'password': 'change me'
        }


    @errbot.botcmd
    def kill_liquidsoap(self, message, _):
        """Try to kill any running liquidsoap process."""
        self._validate_message_from_admin(message)
        subprocess.run(["pkill", "liquidsoap"])
        self._liquidsoap_process = None
        return 'I tried to kill liquidsoap'


    @errbot.botcmd
    def stop_liquidsoap(self, message, _):
        """Stop the running liquidsoap process."""
        self._validate_message_from_admin(message)
        try:
            result = self._stop_liquidsoap()
        # FIXME: using LiquidsoapNotRunningError here makes the test fail.
        # --elopio - 20200916
        except Exception as exception:
            return 'I was not running liquidsoap.'
        else:
            if result:
                return 'I have stopped liquidsoap.'
            else:
                raise LiquidsoapError()


    def _validate_message_from_admin(self, message):
        if (not message.is_group or
                message.frm.room.id != self.config['admin_group_id']):
            raise LiquidsoapNotFromAdminError()


    def _stop_liquidsoap(self):
        """Stop the running liquidsoap process.

        Return True if the process was stopped.
        Raise LiquidsoapNotRunningError if the liquidsoap process was not
        running.
        """
        try:
            self._liquidsoap_process.kill()
        except AttributeError:
            raise LiquidsoapNotRunningError()
        else:
            return True


    @errbot.botcmd
    def stream_radio(self, message, stream_address):
        """Retransmit a radio stream."""
        # The stop command validates that the message comes from the admin
        # group.
        self.stop_liquidsoap(message, None)
        self._stream_radio(stream_address)
        return 'I have started the stream.'


    def _stream_radio(self, stream_address):
        """Retransmit a radio stream."""
        if stream_address.startswith('https://'):
            script = 'stream-https.liq'
        elif stream_address.startswith('http://'):
            script = 'stream-http.liq'
        else:
            raise LiquidsoapUnsupportedProtocolError(stream_address)

        self._run_liquidsoap_process(script, stream_address)


    def _run_liquidsoap_process(self, script_name, *args):
        self._liquidsoap_process = subprocess.Popen([
            'liquidsoap',
            os.path.join(self._scripts_path, script_name),
            '--',
            self.config['host'],
            self.config['port'],
            self.config['mount'],
            self.config['user'],
            self.config['password'],
            *args
        ])


    @errbot.arg_botcmd('playlist_address', type=str)
    def stream_playlist(self, message, playlist_address):
        """Stream a playlist."""
        # The stop command validates that the message comes from the admin
        # group.
        self.stop_liquidsoap(message, None)
        self._stream_playlist(playlist_address)
        return 'I have started the playlist stream.'


    def _stream_playlist(self, playlist_address):
        self._run_liquidsoap_process(
            'stream-playlist.liq',
            playlist_address
        )


    @errbot.arg_botcmd('file_address', type=str)
    def stream_file(self, message, file_address):
        """Stream a file."""
        # The stop command validates that the message comes from the admin
        # group.
        self.stop_liquidsoap(message, None)
        # TODO: unit test.
        # --elopio - 20201128
        file_local_path = self._download_file(file_address)
        self._stream_file(file_local_path)
        yield 'I have started the file stream.'
        # TODO: make non-blocking.
        # --elopio - 20201128
        self._liquidsoap_process.wait()
        os.remove(file_local_path)


    def _stream_file(self, file_path):
        self._run_liquidsoap_process('stream-file.liq', file_path)


    def _download_file(self, file_address):
        parsed_url = urllib.parse.urlparse(file_address)
        _, extension = os.path.splitext(parsed_url.path)
        with tempfile.NamedTemporaryFile(
                delete=False, suffix=extension) as out_file:
            # copied from https://stackoverflow.com/a/7244263
            with urllib.request.urlopen(file_address) as response:
                shutil.copyfileobj(response, out_file)

        return out_file.name
