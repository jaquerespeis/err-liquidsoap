import os
import subprocess

import testtools

import liquidsoap

from unittest import mock

from errbot.backends import test


class LiquidsoapBotCommandsTestCase(testtools.TestCase):


    def setUp(self):
        super().setUp()
        self.testbot = test.TestBot(
            extra_plugin_dir=os.path.join(os.path.dirname(__file__), '..'),
            extra_config={'CORE_PLUGINS': None})
        self.testbot.start()
        self.addCleanup(self.testbot.stop)

        self.testbot.inject_mocks(
            'Liquidsoap',
            {'_validate_message_from_admin': mock.MagicMock(
                return_value=True)})


    def test_stop_liquidsoap_without_running_process(self):
        self.testbot.inject_mocks(
            'Liquidsoap',
            {'_stop_liquidsoap': mock.MagicMock(
                side_effect=liquidsoap.LiquidsoapNotRunningError)}
        )
        self.testbot.push_message('!stop_liquidsoap')
        self.assertEqual(
            'I was not running liquidsoap.',
            self.testbot.pop_message())


    def test_stop_liquidsoap_with_running_process(self):
        self.testbot.inject_mocks(
            'Liquidsoap',
            {'_stop_liquidsoap': mock.MagicMock(return_value=True)}
        )
        self.testbot.push_message('!stop_liquidsoap')
        self.assertEqual(
            'I have stopped liquidsoap.',
            self.testbot.pop_message())


    def test_stream_radio(self):
        mock_stream_radio = mock.MagicMock()
        self.testbot.inject_mocks(
            'Liquidsoap', {'_stream_radio': mock_stream_radio})
        self.testbot.push_message('!stream_radio test-stream')
        self.assertEqual(
            'I have started the stream.',
            self.testbot.pop_message())
        mock_stream_radio.assert_called_with('test-stream')


    def test_stream_playlist(self):
        mock_stream_playlist = mock.MagicMock()
        self.testbot.inject_mocks(
            'Liquidsoap', {'_stream_playlist': mock_stream_playlist})
        self.testbot.push_message(
            '!stream_playlist test-address --duration 60')
        self.assertEqual(
            'I have started the playlist stream.',
            self.testbot.pop_message())
        mock_stream_playlist.assert_called_with('test-address', 60)


    def test_stream_file(self):
        mock_stream_file = mock.MagicMock()
        self.testbot.inject_mocks(
            'Liquidsoap', {'_stream_file': mock_stream_file})
        self.testbot.push_message(
            '!stream_file test-address')
        self.assertEqual(
            'I have started the file stream.',
            self.testbot.pop_message())
        mock_stream_file.assert_called_with('test-address')


class LiquidsoapActionsTestCase(testtools.TestCase):


    def setUp(self):
        super().setUp()
        self.liquidsoap = liquidsoap.Liquidsoap(mock.MagicMock())
        self.liquidsoap.config = {
            'host': 'test-host',
            'port': 'test-port',
            'mount': 'test-mount',
            'user': 'test-user',
            'password': 'test-password'
        }


    def test_stop_liquidsoap_without_running_process(self):
        with testtools.ExpectedException(liquidsoap.LiquidsoapNotRunningError):
            self.liquidsoap._stop_liquidsoap()


    def test_stop_liquidsoap_with_running_process(self):
        with mock.patch.object(
                self.liquidsoap,
                '_liquidsoap_process',
                spec=subprocess.Popen) as mock_process:
            result = self.liquidsoap._stop_liquidsoap()
        mock_process.kill.assert_called_once_with()
        self.assertTrue(result)


    def test_stream_radio_http(self):
        with mock.patch('subprocess.Popen') as mock_popen:
            self.liquidsoap._stream_radio('http://test-radio')
        mock_popen.assert_called_once_with([
            'liquidsoap',
            os.path.abspath(
                os.path.join(
                    os.path.dirname(__file__),
                    '..',
                    'scripts',
                    'stream-http.liq')),
            '--',
            'test-host',
            'test-port',
            'test-mount',
            'test-user',
            'test-password',
            'http://test-radio'
        ])


    def test_stream_radio_https(self):
        with mock.patch('subprocess.Popen') as mock_popen:
            self.liquidsoap._stream_radio('https://test-radio')
        mock_popen.assert_called_once_with([
            'liquidsoap',
            os.path.abspath(
                os.path.join(
                    os.path.dirname(__file__),
                    '..',
                    'scripts',
                    'stream-https.liq')),
            '--',
            'test-host',
            'test-port',
            'test-mount',
            'test-user',
            'test-password',
            'https://test-radio'
        ])


    def test_stream_radio_unkown_protocol(self):
        with testtools.ExpectedException(
                liquidsoap.LiquidsoapUnsupportedProtocolError) as exception:
            self.liquidsoap._stream_radio('unknown-protocol')


    def test_stream_playlist(self):
        with mock.patch('subprocess.Popen') as mock_popen:
            self.liquidsoap._stream_playlist(
                'test-playlist', 'test-duration')
        mock_popen.assert_called_once_with([
            'liquidsoap',
            os.path.abspath(
                os.path.join(
                    os.path.dirname(__file__),
                    '..',
                    'scripts',
                    'stream-playlist.liq')),
            '--',
            'test-host',
            'test-port',
            'test-mount',
            'test-user',
            'test-password',
            'test-playlist',
            'test-duration'
        ])
